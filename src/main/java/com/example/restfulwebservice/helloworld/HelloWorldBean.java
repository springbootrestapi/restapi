package com.example.restfulwebservice.helloworld;

//lombok

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //getter,setter
@AllArgsConstructor //매개변수가 모두 있는 생성자
@NoArgsConstructor // 기본생성자
public class HelloWorldBean {
    private String message;

}
